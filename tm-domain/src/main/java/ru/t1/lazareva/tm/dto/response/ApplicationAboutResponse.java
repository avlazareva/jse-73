package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class ApplicationAboutResponse extends AbstractResponse {

    private String email;

    private String name;

    private String gitBranch;

    private String gitCommitId;

    private String gitCommitterName;

    private String gitCommitterEmail;

    private String gitCommitMessage;

    private String gitCommitTime;

}
