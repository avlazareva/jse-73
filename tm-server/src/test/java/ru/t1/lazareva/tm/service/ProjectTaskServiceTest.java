package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.lazareva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.entity.TaskNotFoundException;
import ru.t1.lazareva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.lazareva.tm.exception.field.TaskIdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.repository.dto.ProjectDtoRepository;
import ru.t1.lazareva.tm.repository.dto.TaskDtoRepository;
import ru.t1.lazareva.tm.repository.dto.UserDtoRepository;

import static ru.t1.lazareva.tm.constant.ProjectTestData.NON_EXISTING_PROJECT_ID;
import static ru.t1.lazareva.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.lazareva.tm.constant.TaskTestData.NON_EXISTING_TASK_ID;
import static ru.t1.lazareva.tm.constant.TaskTestData.USER_TASK1;
import static ru.t1.lazareva.tm.constant.UserTestData.USER_1;
import static ru.t1.lazareva.tm.constant.UserTestData.USER_2;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private IProjectTaskDtoService service;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @NotNull
    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @Autowired
    private UserDtoRepository user_repository;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() {
        user_repository.add(USER_1);
        user_repository.add(USER_2);
    }

    @After
    public void after() throws Exception {
        taskDtoRepository.clear();
        projectDtoRepository.clear();
        user_repository.clear();
    }

    @Test
    public void bindTaskToProject() throws Exception {
        projectDtoRepository.create(USER_1.getId(), USER_PROJECT1.getName());
        taskDtoRepository.create(USER_1.getId(), USER_TASK1.getName());
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(USER_1.getId(), null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(USER_1.getId(), "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(USER_1.getId(), NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDto task = taskDtoRepository.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId());
    }

    @Test
    public void removeProjectById() throws Exception {
        projectDtoRepository.create(USER_1.getId(), USER_PROJECT1.getName());
        taskDtoRepository.create(USER_1.getId(), USER_TASK1.getName());
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById("", USER_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USER_1.getId(), null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USER_1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.removeProjectById(USER_1.getId(), NON_EXISTING_PROJECT_ID);
        });
        service.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        service.removeProjectById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            projectDtoRepository.findOneById(USER_PROJECT1.getId());
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            projectDtoRepository.findOneById(USER_TASK1.getId());
        });
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        projectDtoRepository.create(USER_1.getId(), USER_PROJECT1.getName());
        taskDtoRepository.create(USER_1.getId(), USER_TASK1.getName());
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_1.getId(), null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_1.getId(), "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.unbindTaskFromProject(USER_1.getId(), NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDto task = taskDtoRepository.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

}
