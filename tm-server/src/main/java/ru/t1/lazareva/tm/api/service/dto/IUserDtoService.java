package ru.t1.lazareva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDto> {

    @NotNull
    UserDto create(@Nullable final String login, @Nullable final String password) throws Exception;

    @NotNull
    UserDto create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception;

    @NotNull
    UserDto create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception;

    @Nullable
    UserDto findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    UserDto findByEmail(@Nullable final String email) throws Exception;

    Boolean isLoginExists(@Nullable final String login) throws Exception;

    Boolean isEmailExists(@Nullable final String email) throws Exception;

    void lockUserByLogin(@Nullable final String login) throws Exception;

    void removeByLogin(@Nullable final String login) throws Exception;

    UserDto setPassword(@Nullable final String id, @Nullable final String password) throws Exception;

    void unlockUserByLogin(@Nullable final String login) throws Exception;

    UserDto update(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) throws Exception;

}
