package ru.t1.lazareva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.IProjectDtoService;
import ru.t1.lazareva.tm.exception.IdEmptyException;
import ru.t1.lazareva.tm.exception.NameEmptyException;
import ru.t1.lazareva.tm.exception.UserIdEmptyException;
import ru.t1.lazareva.tm.model.ProjectDTO;
import ru.t1.lazareva.tm.repository.ProjectDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDtoService implements IProjectDtoService {

    @Nullable
    @Autowired
    private ProjectDtoRepository repository;

    @Override
    @Transactional
    public void save(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new IdEmptyException();
        if (project.getName() == null || project.getName().isEmpty()) throw new NameEmptyException();
        project.setUserId(userId);
        repository.save(project);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final String userId, @Nullable final Collection<ProjectDTO> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null) throw new IdEmptyException();
        if (projects.isEmpty()) throw new IdEmptyException();
        for (@NotNull ProjectDTO projectDTO : projects) {
            if (projectDTO.getName() == null || projectDTO.getName().isEmpty()) throw new NameEmptyException();
            projectDTO.setUserId(userId);
        }
        repository.saveAll(projects);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByIdAndUserId(id, userId);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId);
    }

}