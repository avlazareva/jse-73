package ru.t1.lazareva.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.t1.lazareva.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "role")
public class RoleDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    @JoinColumn(nullable = false, name = "user_id")
    private UserDTO users;

    @Enumerated(EnumType.STRING)
    @Column(name = "roletype")
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }

}