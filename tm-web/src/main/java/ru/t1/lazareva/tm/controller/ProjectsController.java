package ru.t1.lazareva.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.lazareva.tm.api.IProjectDtoService;
import ru.t1.lazareva.tm.model.CustomUser;
import ru.t1.lazareva.tm.repository.ProjectDtoRepository;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectDtoService projectService;

    @GetMapping("/projects")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("project-list", "projects", projectService.findAll(user.getUserId()));
    }

}