package ru.t1.lazareva.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER;

}