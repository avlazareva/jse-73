package ru.t1.lazareva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.request.ProjectListRequest;
import ru.t1.lazareva.tm.dto.response.ProjectListResponse;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.event.ConsoleEvent;
import ru.t1.lazareva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSortType(sort);
        @Nullable final ProjectListResponse response = projectEndpoint.listProject(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @NotNull final List<ProjectDto> projects = response.getProjects();
        renderProjects(projects);
    }

    private void renderProjects(@NotNull final List<ProjectDto> projects) {
        int index = 1;
        for (@Nullable final ProjectDto project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}